FROM node:latest

RUN apt-get -qq update \
	&& apt-get install apt-utils --assume-yes

RUN apt-get install zip --assume-yes

RUN apt-get install curl --assume-yes

RUN apt-get install ruby-dev --assume-yes \
	&& apt-get install rubygems --assume-yes \
	&& gem update --system \
	&& gem install bundler